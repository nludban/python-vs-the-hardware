#!/usr/local/bin/python3.6
#
# TTX Tech Turbo Gamepad (Dual Vibration)
#
#ugen0.7: <DragonRise Inc. Generic   USB  Joystick> at usbus0
#uhid2 on uhub0
#uhid2: <DragonRise Inc. Generic   USB  Joystick, class 0/0, rev 1.00/1.07, addr 7> on usbus0
#

import sys
import time

import usb1
import libusb1

ctx = usb1.USBContext()

class TurboGamepad:

    N = {
        0x00 : '-',
        0x10 : '1',
        0x20 : '2',
        0x40 : '3',
        0x80 : '4',
        }

    D = {
        0x0f : '--',
        0x00 : 'NN',
        0x01 : 'NE',
        0x02 : 'EE',
        0x03 : 'SE',
        0x04 : 'SS',
        0x05 : 'SW',
        0x06 : 'WW',
        0x07 : 'NW',
        }

    S = {
        0x00 : '-',
        0x10 : '@',	# select
        0x20 : '*',	# start
        0x40 : '<',
        0x80 : '>',
        }
    #1501866987.516 7f 7f 85 7f 7f 0f 40 c0D=-- S=-- F=--,--,--,--
    #1501866988.372 7f 7f 85 7f 7f 0f 80 c0D=-- S=-- F=--,--,--,--


    F = {
        0x00 : '--',
        0x01 : 'L1',
        0x02 : 'R1',
        0x04 : 'L2',
        0x08 : 'R2',
        }

    def run(self):
        hand = ctx.openByVendorIDAndProductID(0x0079, 0x0006)
        dev = hand.getDevice()

        N = self.N
        D = self.D
        S = self.S
        F = self.F

        if hand.kernelDriverActive(0):
            print('kernel-active!')
            print('detached=', hand.detachKernelDriver(0))
        print('claim=', hand.claimInterface(0))

        #ctx.setDebug(libusb1.LIBUSB_LOG_LEVEL_DEBUG)
        odata = '\0' * 5
        otail = '\0' * 3

        while True:
            try:
                ndata = hand.interruptRead(0x81, 0x8)
                now = time.time()
                if odata != ndata:
                    #print '%.3f %s' % ( now, ' '.join([ '%02x' % d #ord(d)
                    #                                    for d in ndata ]) )

                    ## Digital:
                    # L=7f,7f R=7f,7f N=---- D=-- S=-- F=--,--,--,--
                    #
                    # LX LY MM 7f 7f - Nf SF c0
                    # LX = 00 (left) 7f (mid) ff (right)
                    # LY = 00 (up) 7f (mid) ff (down)
                    # MM = 86 (mid); LX + RY (11-e8)
                    # -
                    # N = 4321 (right numbers and analog)
                    # S = ?:?:start:select
                    # F = R2:L2:R1:L1

                    ## Analog (fully push switch):
                    # L=80,80 R=80,80 N=---- D=-- S=-- F=--,--,--,--
                    #
                    # LX LY MM RX RY - ND SF 40
                    # LX = 00/80/ff
                    # LY = 00/80/ff
                    # MM = LX+RY
                    # RX = 00/80/ff
                    # RY = 00/80/ff
                    # -
                    # N = 4321 (right numbers and analog)
                    # D = 0-7 (n/ne/e/se/s/sw/w/nw), f=none
                    # S = ?:?:start:select
                    # F = R2:L2:R1:L1

                    if len(ndata) not in ( 5, 8 ):
                        print('\nXXX %s' % ':'.join([ '%02x' % ord(d)
                                                      for d in ndata ]))
                        continue

                    if len(ndata) == 8:
                        otail = ndata[:-3]
                    else:
                        ndata += otail

                    lx, ly, mm, rx, ry, nd, sf, cc = [ d for d in ndata ]
                    buf = '%.3f L=%02x,%02x R=%02x,%02x N=%s D=%s S=%s F=%s\r' % (
                        now,
                        lx, ly,
                        rx, ry,
                        ''.join(( N[nd & 0x80],
                                  N[nd & 0x40],
                                  N[nd & 0x20],
                                  N[nd & 0x10] )),
                        D[nd & 0x0f],
                        ''.join(( S[sf & 0x40],
                                  S[sf & 0x10],
                                  S[sf & 0x20],
                                  S[sf & 0x80] )),
                        ','.join(( F[sf & 0x04],
                                   F[sf & 0x01],
                                   F[sf & 0x02],
                                   F[sf & 0x08] )),
                        )
                    sys.stdout.write(buf)
                    sys.stdout.flush()
                    odata = ndata

            except libusb1.USBError as err:
                print(err)


if (__name__ == '__main__'):
    TurboGamepad().run()

#--#
