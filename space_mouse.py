#!/usr/local/bin/python3.6
#
# 3Dconnexion SpaceMouse Compact
#

import struct
import sys
import time

import usb1
import libusb1

ctx = usb1.USBContext()

ESC = '\033'
assert len(ESC) == 1

class SpaceMouse:

    def run(self):
        hand = ctx.openByVendorIDAndProductID(0x256f, 0xc635)
        dev = hand.getDevice()

        if hand.kernelDriverActive(0):
            print('kernel-active!')
            print('detached=', hand.detachKernelDriver(0))
        print('claim=', hand.claimInterface(0))

        #ctx.setDebug(libusb1.LIBUSB_LOG_LEVEL_DEBUG)
        odata = '\0' * 5
        otail = '\0' * 3

        print('\n' * 3)
        pb = 0
        px = py = pz = 0
        rx = ry = rz = 0

        while True:
            try:

                ndata = hand.interruptRead(0x81, 0x7)
                now = time.time()
                if odata == ndata:
                    continue

                #print('%.3f %s' % ( now, ' '.join([ '%02x' % d #ord(d)
                #                                    for d in ndata ]) ))

                if ndata[0] == 0x01:
                    # Report 1: position.
                    px, py, pz = struct.unpack('<hhh', ndata[1:7])
                    #print(f'P= {px:+8} {py:+8} {pz:+8}')

                if ndata[0] == 0x02:
                    # Report 2: rotation.
                    rx, ry, rz = struct.unpack('<hhh', ndata[1:7])
                    #print(f'R= {rx:+8} {ry:+8} {rz:+8}')

                if ndata[0] == 0x03:
                    # Report 3: buttons!
                    pb, = struct.unpack('<H', ndata[1:3])
                    #print(f'B= {hex(pb):8}')

                print((ESC + '[F') * 4)			# ??? 3x does'nt work.
                print(f'Buttons : {hex(pb):8}')
                # ~350+/- 6x axes
                print(f'Position: {px:+8} {py:+8} {pz:+8}')
                print(f'Rotation: {rx:+8} {ry:+8} {rz:+8}')
                sys.stdout.flush()

                odata = ndata

            except libusb1.USBError as err:
                print(err)


if (__name__ == '__main__'):
    SpaceMouse().run()

#--#
