#######################################################################
python-vs-the-hardware/README.rst
#######################################################################

This was a presentation given at the central ohio python meetup on
2017-08-28.  Clone the repo and view presentation.html in a browser to
view (make the font bigger and resize the window so the blue headers
make minimum height "slides").

The source C extension shown in the presentation is available at
https://bitbucket.org/nludban/freebsd-python-iic/
but note it's still in early development.

Look for 3D Space Mouse in the files, it was not part of the original
presentation.

***********************************************************************
Abstract
***********************************************************************

Found an interesting USB HID (Human Interface Device) but don't have a
driver for Raspberry Pi, or Python?  Maybe you found an interesting HAT
with Python drivers, but you prefer to run FreeBSD on the Pi?
This presentation illustrates the development process for a few such
examples using USB command line utilities (borrowed from Linux), Python
libusb1 package, and creating new C extensions.

Target audience is beginner-intermediate; purpose of the presentation
is to show the development process rather than the finished product.

(Spoiler alert: Python wins.)
