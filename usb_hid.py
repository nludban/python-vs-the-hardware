#!/usr/local/bin/python3.6
#
# turbo gamepad:
# sudo ./usb_hid.py 0x79 0x6 0x81 0x8
#
# thrustmaster:
# sudo ./usb_hid.py 0x7b5 0x316 0x81 0x8
#
import sys
import time

import usb1
import libusb1

ctx = usb1.USBContext()

def run(vendor_id, product_id, endpoint, nbytes):
    hand = ctx.openByVendorIDAndProductID(vendor_id, product_id)
    dev = hand.getDevice()

    if hand.kernelDriverActive(0):
        print('kernel-active!')
        print('detached=', hand.detachKernelDriver(0))
        print('reset=', hand.resetDevice())
    print('claim=', hand.claimInterface(0))

    #ctx.setDebug(libusb1.LIBUSB_LOG_LEVEL_DEBUG)

    odata = None
    while True:
        try:
            #ndata = hand.interruptRead(endpoint=129, length=256)
            ndata = hand.interruptRead(endpoint, nbytes)
            now = time.time()
            if odata != ndata:
                print('%.3f -- %s' % ( now, ' '.join([ '%02x' % d
                                                       for d in ndata ]) ))
                odata = ndata
        except libusb1.USBError as err:
            print(err)


arg0, vendor_id, product_id, endpoint, nbytes = sys.argv
vendor_id = int(vendor_id, 16)
product_id = int(product_id, 16)
endpoint = int(endpoint, 16)
nbytes = int(nbytes, 16)
run(vendor_id, product_id, endpoint, nbytes)

#--#
