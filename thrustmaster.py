#!/usr/local/bin/python3.6

import sys
import time

import usb1
import libusb1

ctx = usb1.USBContext()

class ThrustMaster:

    def run(self):
        hand = ctx.openByVendorIDAndProductID(0x07b5, 0x0316)
        print(hand)
        dev = hand.getDevice()

        dump_device(dev)
        if hand.kernelDriverActive(0):
            print('kernel-active!')
            print('detached=', hand.detachKernelDriver(0))
        print('reset=', hand.resetDevice())
        print('claim=', hand.claimInterface(0))

        #ctx.setDebug(libusb1.LIBUSB_LOG_LEVEL_DEBUG)


        #1501862234.648 ff 00 3f d2 00 00
        #1501862234.688 00 00 3f d2 00 00
        #1501862234.808 01 00 3f d2 00 00
        #               ^^
        # X position (left < center=0 < right)


        #1501862290.391 00 ff 3f d2 00 00
        #1501862290.447 00 00 3f d2 00 00
        #1501862290.567 00 01 3f d2 00 00
        #                  ^^
        # Y position (forward < center=0 < back)

        #1501862416.132 00 00 8f 80 00 00
        #1501862416.156 00 00 e4 8e 00 00
        #1501862416.172 00 00 59 de 00 00
        #1501862416.196 00 00 7f 00 00 00
        #                     ^^/^^
        # Slider position A/B
        #	A = forward < center=0 < back
        #	B = forward < back=0

        #1501862152.490 00 00 3f d2 01 00
        #1501862150.578 00 00 3f d2 08 00
        #                            ^ POV 1=N, 2=NE, 3=E...

        #1501862983.399 00 00 7f 00 10 00
        #                           ^
        # B1 = Front trigger.

        #1501863011.222 00 00 7f 00 20 00
        #                           ^
        # B2 = Hazard button.

        #1501863034.382 00 00 7f 00 80 00
        #                           ^
        # B4 = Button.

        # XXX
        #Note Force feedback devices requiring real time interaction are covered in a
        #separate document titled "USB Physical Interface Device (PID) Class."

        odata = None
        while True:
            try:
                ndata = hand.interruptRead(0x81, 0x8)
                print 'OK.'
                now = time.time()
                if True or odata != ndata:
                    print '%.3f %s' % ( now, ' '.join([ '%02x' % d #ord(d)
                                                        for d in ndata ]) )

                    odata = ndata
            except libusb1.USBError as err:
                print(err)


#---------------------------------------------------------------------#

###hand.interruptRead(129, 8)
#libusbx: error [submit_bulk_transfer] submiturb failed error -1 errno=16
#libusb1.USBError: LIBUSB_ERROR_IO [-1]
#define EBUSY           16      /* Device or resource busy */

###hand.interruptRead(0, 8)
#define ENOENT           2      /* No such file or directory */
#[212571.323582] usb 1-1.2.4.1: usbfs: process 6948 (python) did not claim interface 0 before use

#http://stackoverflow.com/questions/11088691/error-message-interface-not-claimed-from-libusb

if (__name__ == '__main__'):
    #dump_devices()
    TurboGamepad().run()
    #GamepadPro().run()
    #WacomTablet().run()
    #ThrustMaster().run()

#--#
#https://github.com/bashwork/pymodbus - older, BSD?
#https://code.google.com/p/modbus-tk/ - bigger, LGPL?
#MinimalModbus - serial only
